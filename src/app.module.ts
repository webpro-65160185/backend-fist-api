import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { temperatureModule } from './temperature/temperature.module';
import { UsersModule } from './users/users.module';

@Module({
  imports: [temperatureModule, UsersModule],
  controllers: [AppController],
  providers: [AppService],
  exports: [],
})
export class AppModule {}
